package me.applejuiceyt.survivalgames;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Kyle on 6/15/2015. All data provided in the following code
 * will be direct property of AppleJuiceYT and is forbidden for redistribution without consent.
 */
public class Setup implements Listener {

    //add custom config files (Maps,
    public void setupInitialSG() {
        //Setup Config
        Main.getPlugin().getConfig().set("setup", true);
        Main.getPlugin().getConfig().set("version", Main.version); //Version from plugin.yml
        Main.getPlugin().getConfig().set("name", "SG1");
        Main.getPlugin().getConfig().set("min-players", 12);
        Main.getPlugin().getConfig().set("max-players", 24);
        Main.getPlugin().getConfig().set("Lobby.Spawn.X", 0);
        Main.getPlugin().getConfig().set("Lobby.Spawn.Y", 0);
        Main.getPlugin().getConfig().set("Lobby.Spawn.Z", 0);
        Main.getPlugin().getConfig().set("Lobby.Spawn.World", "world");
        Main.getPlugin().getConfig().set("lobby-timer", 90);
        Main.getPlugin().saveConfig();



    }

    public static FileConfiguration maps;
    public static File mfile;

    public static void setup(Plugin p) {
        mfile = new File(p.getDataFolder(), "maps.yml");
        if (!mfile.exists()) {
            try {
                mfile.createNewFile();
            } catch(IOException e) {
                Bukkit.getServer().getLogger().severe(Main.prefix + ChatColor.RED + "" + ChatColor.BOLD + "Could not create maps.yml!");
            }
        }

        maps = YamlConfiguration.loadConfiguration(mfile);
    }

    public static FileConfiguration getMaps() {
        return maps;
    }


    public static void saveMaps() {
        try {
            maps.save(mfile);
        } catch (IOException e) {
            Bukkit.getServer().getLogger().severe(Main.prefix + ChatColor.RED + "" + ChatColor.BOLD + "Could not save maps.yml!");
        }
    }



    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        if (Main.getPlugin().getConfig().getBoolean("setup") == false) {
            e.getPlayer().kickPlayer(ChatColor.RED + "" + ChatColor.BOLD + "Setting up server for you...");
            setupInitialSG();
        }
    }
}