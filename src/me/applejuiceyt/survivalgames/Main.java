package me.applejuiceyt.survivalgames;

import me.applejuiceyt.survivalgames.Cmds.SG;
import me.applejuiceyt.survivalgames.Events.*;
import me.applejuiceyt.survivalgames.Game.GState;
import me.applejuiceyt.survivalgames.Game.GameState;
import me.applejuiceyt.survivalgames.Lobby.JoinLobby;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;


public class Main extends JavaPlugin {


    //FIRST TIME INSTALL:
    /*
    Check if config setup is null then setup config files and set default data
    Kick player while setup is happening
    When they join back treat it like its a real game and all setup. (TP to lobby..etc)

     */


    private static Main plugin;
    public static String prefix = ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "Survival Games" + ChatColor.DARK_GRAY + "] ";

    public static Main getPlugin() {
        return plugin;
    }



    public static String version;
    public void onEnable() {
        plugin = this;

        SG cmd = new SG();
        getCommand("sg").setExecutor(cmd);

        GameState lobby = GameState.LOBBY;
        GState.setState(lobby);



        PluginDescriptionFile pdf = this.getDescription(); //Gets plugin.yml
        version = pdf.getVersion();

        Bukkit.getPluginManager().registerEvents(new Setup(), this);
        Bukkit.getPluginManager().registerEvents(new BlockPlace(), this);
        Bukkit.getPluginManager().registerEvents(new Damage(), this);
        Bukkit.getPluginManager().registerEvents(new JoinLobby(), this);
        Bukkit.getPluginManager().registerEvents(new DropItem(), this);
        Bukkit.getPluginManager().registerEvents(new Pickup(), this);
        Bukkit.getPluginManager().registerEvents(new BlockBreak(), this);
        Bukkit.getPluginManager().registerEvents(new Hungry(), this);
        Bukkit.getPluginManager().registerEvents(new MobSpawn(), this);
        Bukkit.getPluginManager().registerEvents(new CreeperExplode(), this);
        Bukkit.getPluginManager().registerEvents(new Quit(), this);
        Bukkit.getPluginManager().registerEvents(new Ping(), this);

        Setup.setup(getPlugin());
    }

    public void onDisable() {
        plugin = null;

    }

}