package me.applejuiceyt.survivalgames.Game;

/**
 * Created by Kyle on 6/17/2015. All data provided in the following code
 * will be direct property of AppleJuiceYT and is forbidden for redistribution without consent.
 */
public class GState {
    public static GameState gamestate;

    public static GameState getState() {
        return gamestate;

    }
    public static void setState(GameState state) {
        gamestate = state;
    }
}
