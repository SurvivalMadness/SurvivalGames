package me.applejuiceyt.survivalgames.Game;

/**
 * Created by Kyle on 6/17/2015. All data provided in the following code
 * will be direct property of AppleJuiceYT and is forbidden for redistribution without consent.
 */
public enum GameState {
    LOBBY("Lobby"), INGAME("InGame"), RESTARTING("Restarting");

    String state;
    GameState(String state) {
        this.state = state;
    }




}
