package me.applejuiceyt.survivalgames.Lobby;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

/**
 * Created by Kyle on 6/17/2015. All data provided in the following code
 * will be direct property of AppleJuiceYT and is forbidden for redistribution without consent.
 */
public class Hotbar {


    public static void setHotbar(Player p) {
        p.getInventory().clear();
        p.getInventory().setArmorContents(null);

        ItemStack bth = new ItemStack(Material.WATCH);
        ItemMeta bthm = bth.getItemMeta();
        bthm.setDisplayName(ChatColor.DARK_RED + "" + ChatColor.BOLD + "Back To Hub!");
        bthm.setLore(Arrays.asList(
                ChatColor.GRAY + "Teleport back to the hub!"
        ));
        bth.setItemMeta(bthm);

        ItemStack vote = new ItemStack(Material.PAPER);
        ItemMeta vm = vote.getItemMeta();
        vm.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "/vote");
        vm.setLore(Arrays.asList(
                ChatColor.GRAY + "Vote for your favourite map!"
        ));
        vote.setItemMeta(vm);

        p.getInventory().setItem(8, bth);
        p.getInventory().setItem(7, vote);

    }
}
