package me.applejuiceyt.survivalgames.Lobby;

import me.applejuiceyt.survivalgames.Game.GState;
import me.applejuiceyt.survivalgames.Game.GameState;
import me.applejuiceyt.survivalgames.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Kyle on 6/17/2015. All data provided in the following code
 * will be direct property of AppleJuiceYT and is forbidden for redistribution without consent.
 */
public class Timer {

    static int countdown = Main.getPlugin().getConfig().getInt("lobby-timer");
    public static int timeleft;
    public static void startLobbyTimer() {
        new BukkitRunnable() {
            @Override
            public void run() {
                if (Bukkit.getOnlinePlayers().size() >= Main.getPlugin().getConfig().getInt("min-players")) {

                    if (countdown == 90) {
                        Bukkit.broadcastMessage(Main.prefix + ChatColor.GREEN + "Starting game in 90 seconds!");
                    }
                    countdown--;
                    timeleft = countdown;
                    if (countdown == 60) {
                        Bukkit.broadcastMessage(Main.prefix + ChatColor.GREEN + "Starting game in 60 seconds!");
                    }
                    if (countdown == 30) {
                        Bukkit.broadcastMessage(Main.prefix + ChatColor.GREEN + "Starting game in 30 seconds!");
                    }
                    if (countdown == 15) {
                        Bukkit.broadcastMessage(Main.prefix + ChatColor.GREEN + "Starting game in 15 seconds!");
                    }
                    if (countdown == 10) {
                        Bukkit.broadcastMessage(Main.prefix + ChatColor.GREEN + "Starting game in 10 seconds!");

                    }
                    if (countdown == 5) {
                        Bukkit.broadcastMessage(Main.prefix + ChatColor.GREEN + "Starting game in 5 seconds!");
                    }
                    if (countdown == 4) {
                        Bukkit.broadcastMessage(Main.prefix + ChatColor.GREEN + "Starting game in 4 seconds!");
                    }
                    if (countdown == 3) {
                        Bukkit.broadcastMessage(Main.prefix + ChatColor.GREEN + "Starting game in 3 seconds!");
                    }
                    if (countdown == 2) {
                        Bukkit.broadcastMessage(Main.prefix + ChatColor.GREEN + "Starting game in 2 seconds!");
                    }
                    if (countdown == 1) {
                        Bukkit.broadcastMessage(Main.prefix + ChatColor.GREEN + "Starting game in 1 seconds!");
                    }
                    if (countdown == 0) {
                        Bukkit.broadcastMessage(Main.prefix + ChatColor.GREEN + "" + ChatColor.BOLD + "Starting now!");
                        //GState.setState(GameState.INGAME); TODO ADD THIS WHEN YOU TP
                        //Clear inv
                        //TODO TELEPORT TO GAME
                        //TODO ADD SOUNDS
                        cancel();
                        timeleft = 0;
                    }

                    if (countdown < 0) {
                        timeleft = 0;
                        cancel();
                        timeleft = 0;

                    }

                }
            }
        }.runTaskTimer(Main.getPlugin(),0,20);
    }
}
