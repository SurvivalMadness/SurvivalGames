package me.applejuiceyt.survivalgames.Lobby;


import me.applejuiceyt.survivalgames.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

/**
 * Created by Kyle on 6/17/2015. All data provided in the following code
 * will be direct property of AppleJuiceYT and is forbidden for redistribution without consent.
 */
public class LobbyBoard {

    public static void showLobbyScoreboard(Player p) {

        ScoreboardManager manager = Bukkit.getScoreboardManager();
        Scoreboard board = manager.getNewScoreboard();

        Objective obj = board.registerNewObjective("SG","SG");
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        obj.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "  Survival Games  ");

        Score score1 = obj.getScore(" ");
        Score score2 = obj.getScore(ChatColor.AQUA + "" + ChatColor.BOLD + "Server:");
        Score score3 = obj.getScore(ChatColor.LIGHT_PURPLE + Main.getPlugin().getConfig().getString("name"));
        Score score4 = obj.getScore("  ");
        Score score5 = obj.getScore(ChatColor.YELLOW + "" + ChatColor.BOLD + "Vote:");
        Score score6 = obj.getScore(ChatColor.LIGHT_PURPLE + "/vote " + ChatColor.GREEN + "{MAP}"); //TODO Change to map name when they do vote
        Score score7 = obj.getScore("   ");
        Score score8 = obj.getScore(ChatColor.RED + "" + ChatColor.BOLD + "Players:");
        Score score9 = obj.getScore(ChatColor.WHITE + JoinLobby.getPlayerCount());





        score1.setScore(9);
        score2.setScore(8);
        score3.setScore(7);
        score4.setScore(6);
        score5.setScore(5);
        score6.setScore(4);
        score7.setScore(3);
        score8.setScore(2);
        score9.setScore(1);

        p.setScoreboard(board);

    }
}
