package me.applejuiceyt.survivalgames.Lobby;

import me.applejuiceyt.survivalgames.Main;
import me.applejuiceyt.survivalgames.Setup;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * Created by Kyle on 6/28/2015. All data provided in the following code
 * will be direct property of AppleJuiceYT and is forbidden for redistribution without consent.
 */
public class Vote {


    public static void addVote(Player voter, String map) {
        if (Setup.getMaps().getStringList("map-names").contains(map)) {
            Setup.maps.set(map + ".votes", Setup.getMaps().getInt(map + ".votes" + 1));


        } else {
            voter.sendMessage(Main.prefix + ChatColor.RED + "This is not a valid map!");
            return;
        }
    }


    public static int getVotes(String map) {
        //UNSTABLE IF USED TO CHECK VOTES OF A NOT KNOWN MAP ONLY USE TO DISPLAY IN CODE
        int votes = Setup.getMaps().getInt(map + ".votes");
        return votes;
    }
}
