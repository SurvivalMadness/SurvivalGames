package me.applejuiceyt.survivalgames.Lobby;

import me.applejuiceyt.survivalgames.API.Title;
import me.applejuiceyt.survivalgames.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;


/**
 * Created by Kyle on 6/16/2015. All data provided in the following code
 * will be direct property of AppleJuiceYT and is forbidden for redistribution without consent.
 */
public class JoinLobby implements Listener {



    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        //Ask for votes
        //Hotbar return to hub, vote
        //TODO HOTBAR


        e.setJoinMessage(null);
        e.setJoinMessage(ChatColor.DARK_GRAY + "[" + ChatColor.GREEN + "" + ChatColor.BOLD + "+" + ChatColor.DARK_GRAY + "] " + ChatColor.YELLOW + e.getPlayer().getName() + " " + getPlayerCount());
        Timer.startLobbyTimer(); //Make more efficient eventually


        for (Player online :Bukkit.getOnlinePlayers()) {
            LobbyBoard.showLobbyScoreboard(online);
        }

        Player p = e.getPlayer();
        World world = Bukkit.getServer().getWorld(Main.getPlugin().getConfig().getString("Lobby.Spawn.World"));
        double x = Main.getPlugin().getConfig().getDouble("Lobby.Spawn.X");
        double y = Main.getPlugin().getConfig().getDouble("Lobby.Spawn.Y");
        double z = Main.getPlugin().getConfig().getDouble("Lobby.Spawn.Z");


        Location lspawn = new Location(world, x,y,z); //Sets spawn
        p.teleport(lspawn);

        Title.sendTitle(e.getPlayer(), "&e&lWelcome to Survival Games", "&a " + e.getPlayer().getName(), 20, 20, 20); // Send 1.8 thingy
        Hotbar.setHotbar(e.getPlayer());

        for (Player online : Bukkit.getOnlinePlayers()) {
            Title.sendActionBar(online, getPlayerCount());
        }



        if (Bukkit.getOnlinePlayers().size() == Main.getPlugin().getConfig().getInt("max-players")) {
            e.getPlayer().kickPlayer(ChatColor.DARK_RED + "" + ChatColor.BOLD + "FULL! " + ChatColor.YELLOW + "This game is full! Please try again later!");
        }


    }


    public static String getPlayerCount() {
        String playercount = ChatColor.GRAY + "(" + ChatColor.GOLD + Bukkit.getOnlinePlayers().size() + ChatColor.GRAY + "/" + ChatColor.GOLD + Main.getPlugin().getConfig().getInt("max-players") + ChatColor.GRAY + ")";
        return playercount;

    }

}