package me.applejuiceyt.survivalgames.Events;

import me.applejuiceyt.survivalgames.Game.GState;
import me.applejuiceyt.survivalgames.Game.GameState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntitySpawnEvent;

/**
 * Created by Kyle on 6/17/2015. All data provided in the following code
 * will be direct property of AppleJuiceYT and is forbidden for redistribution without consent.
 */
public class MobSpawn implements Listener {

    @EventHandler
    public void onSpawn(CreatureSpawnEvent e) {
        if (GState.getState() == GameState.LOBBY) {
            e.setCancelled(true);

        }
    }

    @EventHandler
    public  void onSpawn(EntitySpawnEvent e) {
        if (GState.getState() == GameState.LOBBY) {
            e.setCancelled(true);

        }
    }
}
