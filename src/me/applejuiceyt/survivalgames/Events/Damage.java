package me.applejuiceyt.survivalgames.Events;

import me.applejuiceyt.survivalgames.Game.GState;
import me.applejuiceyt.survivalgames.Game.GameState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

/**
 * Created by Kyle on 6/17/2015. All data provided in the following code
 * will be direct property of AppleJuiceYT and is forbidden for redistribution without consent.
 */
public class Damage implements Listener {

    @EventHandler
    public void onDamage(EntityDamageEvent e){
        if (GState.getState() == GameState.LOBBY) {
            e.setCancelled(true);

        }
    }
}
