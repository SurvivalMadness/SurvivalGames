package me.applejuiceyt.survivalgames.Events;

import me.applejuiceyt.survivalgames.Game.GState;
import me.applejuiceyt.survivalgames.Lobby.JoinLobby;
import me.applejuiceyt.survivalgames.Main;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

/**
 * Created by Kyle on 6/17/2015. All data provided in the following code
 * will be direct property of AppleJuiceYT and is forbidden for redistribution without consent.
 */
public class Ping implements Listener {


    @EventHandler
    public void onPing(ServerListPingEvent e) {
        e.setMaxPlayers(Main.getPlugin().getConfig().getInt("max-players"));
        e.setMotd(getMotd());


    }

    public String getMotd() {
        String motd = ChatColor.GREEN + "" + ChatColor.BOLD + GState.getState().toString() + ChatColor.RED + " | " + JoinLobby.getPlayerCount();
        return motd;
    }
}
