package me.applejuiceyt.survivalgames.Events;

import me.applejuiceyt.survivalgames.Game.GState;
import me.applejuiceyt.survivalgames.Game.GameState;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

/**
 * Created by Kyle on 6/17/2015. All data provided in the following code
 * will be direct property of AppleJuiceYT and is forbidden for redistribution without consent.
 */
public class BlockBreak implements Listener {

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        if (GState.getState() == GameState.LOBBY || GState.getState() == GameState.INGAME) {
            e.setCancelled(true);
            //TODO add breaking of leaves

        } else {
            System.out.println("Issue with States"); // Bug test
        }
    }
}
