package me.applejuiceyt.survivalgames.Events;

import me.applejuiceyt.survivalgames.API.Title;
import me.applejuiceyt.survivalgames.Lobby.JoinLobby;
import me.applejuiceyt.survivalgames.Lobby.LobbyBoard;
import me.applejuiceyt.survivalgames.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Kyle on 6/17/2015. All data provided in the following code
 * will be direct property of AppleJuiceYT and is forbidden for redistribution without consent.
 */
public class Quit implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        e.setQuitMessage(null);
        e.setQuitMessage(ChatColor.DARK_GRAY + "[" + ChatColor.RED + "" + ChatColor.BOLD + "-" + ChatColor.DARK_GRAY + "] " + ChatColor.YELLOW + e.getPlayer().getName() + " " + getPlayerCount());

        for (final Player online : Bukkit.getOnlinePlayers()) {
            Title.sendActionBar(online, getPlayerCount());
            new BukkitRunnable() {
                @Override
                public void run() {
                    LobbyBoard.showLobbyScoreboard(online);
                }
            }.runTaskLater(Main.getPlugin(), 6);
        }

    }

    public String getPlayerCount() {
        int pc = Bukkit.getOnlinePlayers().size() - 1;
        String playercount = ChatColor.GRAY + "(" + ChatColor.GOLD + pc + ChatColor.GRAY + "/" + ChatColor.GOLD + Main.getPlugin().getConfig().getInt("max-players") + ChatColor.GRAY + ")";
        return playercount;
    }
}
