package me.applejuiceyt.survivalgames.Cmds;

import me.applejuiceyt.survivalgames.Lobby.Timer;
import me.applejuiceyt.survivalgames.Lobby.Vote;
import me.applejuiceyt.survivalgames.Main;
import me.applejuiceyt.survivalgames.Setup;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Kyle on 6/16/2015. All data provided in the following code
 * will be direct property of AppleJuiceYT and is forbidden for redistribution without consent.
 */
public class SG implements CommandExecutor {

    //TODO RE-ORDER COMMAND LIST

    ArrayList<UUID> voted = new ArrayList<>();

    @Override
    public boolean onCommand(CommandSender commandSender, Command cmd, String s, String[] args) {
        if (cmd.getName().equalsIgnoreCase("sg")) {
            if (args.length < 1) {
                Player p = (Player) commandSender;
                p.sendMessage(ChatColor.GREEN + "==============[Survival Games]==============");
                p.sendMessage(ChatColor.GREEN + "/sg " + ChatColor.YELLOW + "setlobbyspawn");
                p.sendMessage(ChatColor.GREEN + "/sg " + ChatColor.YELLOW + "setspawn");
                p.sendMessage(ChatColor.GREEN + "/sg " + ChatColor.YELLOW + "vote {MAP}");
                p.sendMessage(ChatColor.GREEN + "/sg " + ChatColor.YELLOW + "version");
                p.sendMessage(ChatColor.GREEN + "/sg " + ChatColor.YELLOW + "timer");
            }

            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("setlobbyspawn")) {
                    Player p = (Player) commandSender;
                    String world = p.getWorld().getName();
                    double x = p.getLocation().getX();
                    double y = p.getLocation().getY();
                    double z = p.getLocation().getZ();
                    Main.getPlugin().getConfig().set("Lobby.Spawn.X", x);
                    Main.getPlugin().getConfig().set("Lobby.Spawn.Y", y);
                    Main.getPlugin().getConfig().set("Lobby.Spawn.Z", z);
                    Main.getPlugin().getConfig().set("Lobby.Spawn.World", world);
                    Main.getPlugin().saveConfig();
                    p.sendMessage(Main.prefix + ChatColor.GREEN + "Successfully set the " + ChatColor.YELLOW + "" + ChatColor.BOLD + "Lobby" + ChatColor.GREEN + " spawn.");
                    p.playSound(p.getLocation(), Sound.BURP, 1, 1);
                    p.setVelocity(new Vector(0,0.3,0));

                }
            }
            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("timer")) {

                    List<String> mtemp = Setup.getMaps().getStringList("map-names");
                    mtemp.add("BreezeIsland");
                    mtemp.add("Japan");
                    mtemp.add("University"); //TODO REMOVE
                    mtemp.add("Holiday");
                    Setup.getMaps().set("map-names", mtemp);

                    Player p = (Player) commandSender;
                    if (Bukkit.getOnlinePlayers().size() >= Main.getPlugin().getConfig().getInt("min-players")) {
                        p.sendMessage(Main.prefix + ChatColor.RED + Timer.timeleft + ChatColor.DARK_GREEN + " seconds left!");
                    } else {
                        p.sendMessage(Main.prefix + ChatColor.GREEN + "This game needs atleast " + ChatColor.RED + Main.getPlugin().getConfig().getInt("min-players") + ChatColor.GREEN + " players to start!");
                    }
                }
            }

            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("vote")) {
                    Player p = (Player) commandSender;

                    p.sendMessage(ChatColor.GREEN + "==============[Vote]==============");
                    if (Setup.getMaps().getStringList("map-names") == null) {
                        p.sendMessage(ChatColor.RED + "" + ChatColor.ITALIC + "No maps have been added yet! Do " + ChatColor.GREEN + "" + ChatColor.ITALIC + "/sg map create {NAME}");
                    } else {

                        for (String name : Setup.getMaps().getStringList("map-names")) {
                            p.sendMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "- " + ChatColor.GOLD + name + ChatColor.GRAY + " [" + ChatColor.LIGHT_PURPLE + Vote.getVotes(name)+ ChatColor.GRAY + "]");
                        }
                    }


                }
            }

            if (args.length == 2) {
                Bukkit.broadcastMessage("Got here bb");
                    Player p = (Player) commandSender;
                    p.sendMessage("Got here1");

                    if (Setup.getMaps().getStringList("map-names").contains(args[1])) {

                        p.sendMessage("got here2");
                        if (!voted.contains(p.getUniqueId())) {
                            p.sendMessage(Main.prefix + ChatColor.GOLD + "You have successfully voted for the map " + ChatColor.GREEN + args[1]);
                            Vote.addVote(p, args[1]);
                            voted.add(p.getUniqueId());
                        } else {
                            p.sendMessage(Main.prefix + ChatColor.RED + "You have already voted!");
                        }

                    } else {
                        p.sendMessage(Main.prefix + ChatColor.RED + "This is not an available map!");
                    }

            }
        }
        return false;
    }
}